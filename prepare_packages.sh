# Copy the ROS  packages to all TurtleBots.
./copy_packages.sh

echo "Compiling the packages on each TurtleBot..."
ssh turtlebot@192.168.1.59 'bash -s' < remote_compile_packages.sh &
ssh turtlebot@192.168.1.58 'bash -s' < remote_compile_packages.sh &
ssh turtlebot@192.168.1.57 'bash -s' < remote_compile_packages.sh &
ssh turtlebot@192.168.1.56 'bash -s' < remote_compile_packages.sh &
ssh turtlebot@192.168.1.55 'bash -s' < remote_compile_packages.sh &
ssh turtlebot@192.168.1.54 'bash -s' < remote_compile_packages.sh &
ssh turtlebot@192.168.1.53 'bash -s' < remote_compile_packages.sh &
ssh turtlebot@192.168.1.52 'bash -s' < remote_compile_packages.sh &
ssh turtlebot@192.168.1.51 'bash -s' < remote_compile_packages.sh &
ssh turtlebot@192.168.1.50 'bash -s' < remote_compile_packages.sh
wait
echo "Finished compiling packages"