/* Software License Agreement (BSD License)
 *
 * Copyright (c) 2011, Willow Garage, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above
 *    copyright notice, this list of conditions and the following
 *    disclaimer in the documentation and/or other materials provided
 *    with the distribution.
 *  * Neither the name of Willow Garage, Inc. nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * Auto-generated by gensrv_cpp from file /home/turtlebot/ros/workspaces/hydro/mvce_ws/src/multimaster_fkie/multimaster_msgs_fkie/srv/GetSyncInfo.srv
 *
 */


#ifndef MULTIMASTER_MSGS_FKIE_MESSAGE_GETSYNCINFO_H
#define MULTIMASTER_MSGS_FKIE_MESSAGE_GETSYNCINFO_H

#include <ros/service_traits.h>


#include <multimaster_msgs_fkie/GetSyncInfoRequest.h>
#include <multimaster_msgs_fkie/GetSyncInfoResponse.h>


namespace multimaster_msgs_fkie
{

struct GetSyncInfo
{

typedef GetSyncInfoRequest Request;
typedef GetSyncInfoResponse Response;
Request request;
Response response;

typedef Request RequestType;
typedef Response ResponseType;

}; // struct GetSyncInfo
} // namespace multimaster_msgs_fkie


namespace ros
{
namespace service_traits
{


template<>
struct MD5Sum< ::multimaster_msgs_fkie::GetSyncInfo > {
  static const char* value()
  {
    return "d5261ec56e202860a07fb47b41e1b2a8";
  }

  static const char* value(const ::multimaster_msgs_fkie::GetSyncInfo&) { return value(); }
};

template<>
struct DataType< ::multimaster_msgs_fkie::GetSyncInfo > {
  static const char* value()
  {
    return "multimaster_msgs_fkie/GetSyncInfo";
  }

  static const char* value(const ::multimaster_msgs_fkie::GetSyncInfo&) { return value(); }
};


// service_traits::MD5Sum< ::multimaster_msgs_fkie::GetSyncInfoRequest> should match 
// service_traits::MD5Sum< ::multimaster_msgs_fkie::GetSyncInfo > 
template<>
struct MD5Sum< ::multimaster_msgs_fkie::GetSyncInfoRequest>
{
  static const char* value()
  {
    return MD5Sum< ::multimaster_msgs_fkie::GetSyncInfo >::value();
  }
  static const char* value(const ::multimaster_msgs_fkie::GetSyncInfoRequest&)
  {
    return value();
  }
};

// service_traits::DataType< ::multimaster_msgs_fkie::GetSyncInfoRequest> should match 
// service_traits::DataType< ::multimaster_msgs_fkie::GetSyncInfo > 
template<>
struct DataType< ::multimaster_msgs_fkie::GetSyncInfoRequest>
{
  static const char* value()
  {
    return DataType< ::multimaster_msgs_fkie::GetSyncInfo >::value();
  }
  static const char* value(const ::multimaster_msgs_fkie::GetSyncInfoRequest&)
  {
    return value();
  }
};

// service_traits::MD5Sum< ::multimaster_msgs_fkie::GetSyncInfoResponse> should match 
// service_traits::MD5Sum< ::multimaster_msgs_fkie::GetSyncInfo > 
template<>
struct MD5Sum< ::multimaster_msgs_fkie::GetSyncInfoResponse>
{
  static const char* value()
  {
    return MD5Sum< ::multimaster_msgs_fkie::GetSyncInfo >::value();
  }
  static const char* value(const ::multimaster_msgs_fkie::GetSyncInfoResponse&)
  {
    return value();
  }
};

// service_traits::DataType< ::multimaster_msgs_fkie::GetSyncInfoResponse> should match 
// service_traits::DataType< ::multimaster_msgs_fkie::GetSyncInfo > 
template<>
struct DataType< ::multimaster_msgs_fkie::GetSyncInfoResponse>
{
  static const char* value()
  {
    return DataType< ::multimaster_msgs_fkie::GetSyncInfo >::value();
  }
  static const char* value(const ::multimaster_msgs_fkie::GetSyncInfoResponse&)
  {
    return value();
  }
};

} // namespace service_traits
} // namespace ros

#endif // MULTIMASTER_MSGS_FKIE_MESSAGE_GETSYNCINFO_H
