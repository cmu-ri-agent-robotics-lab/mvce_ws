/* Software License Agreement (BSD License)
 *
 * Copyright (c) 2011, Willow Garage, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above
 *    copyright notice, this list of conditions and the following
 *    disclaimer in the documentation and/or other materials provided
 *    with the distribution.
 *  * Neither the name of Willow Garage, Inc. nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * Auto-generated by gensrv_cpp from file /home/turtlebot/ros/workspaces/hydro/mvce_ws/src/multimaster_fkie/multimaster_msgs_fkie/srv/LoadLaunch.srv
 *
 */


#ifndef MULTIMASTER_MSGS_FKIE_MESSAGE_LOADLAUNCH_H
#define MULTIMASTER_MSGS_FKIE_MESSAGE_LOADLAUNCH_H

#include <ros/service_traits.h>


#include <multimaster_msgs_fkie/LoadLaunchRequest.h>
#include <multimaster_msgs_fkie/LoadLaunchResponse.h>


namespace multimaster_msgs_fkie
{

struct LoadLaunch
{

typedef LoadLaunchRequest Request;
typedef LoadLaunchResponse Response;
Request request;
Response response;

typedef Request RequestType;
typedef Response ResponseType;

}; // struct LoadLaunch
} // namespace multimaster_msgs_fkie


namespace ros
{
namespace service_traits
{


template<>
struct MD5Sum< ::multimaster_msgs_fkie::LoadLaunch > {
  static const char* value()
  {
    return "e7cc67269dc4de0d6d7a3648f287878b";
  }

  static const char* value(const ::multimaster_msgs_fkie::LoadLaunch&) { return value(); }
};

template<>
struct DataType< ::multimaster_msgs_fkie::LoadLaunch > {
  static const char* value()
  {
    return "multimaster_msgs_fkie/LoadLaunch";
  }

  static const char* value(const ::multimaster_msgs_fkie::LoadLaunch&) { return value(); }
};


// service_traits::MD5Sum< ::multimaster_msgs_fkie::LoadLaunchRequest> should match 
// service_traits::MD5Sum< ::multimaster_msgs_fkie::LoadLaunch > 
template<>
struct MD5Sum< ::multimaster_msgs_fkie::LoadLaunchRequest>
{
  static const char* value()
  {
    return MD5Sum< ::multimaster_msgs_fkie::LoadLaunch >::value();
  }
  static const char* value(const ::multimaster_msgs_fkie::LoadLaunchRequest&)
  {
    return value();
  }
};

// service_traits::DataType< ::multimaster_msgs_fkie::LoadLaunchRequest> should match 
// service_traits::DataType< ::multimaster_msgs_fkie::LoadLaunch > 
template<>
struct DataType< ::multimaster_msgs_fkie::LoadLaunchRequest>
{
  static const char* value()
  {
    return DataType< ::multimaster_msgs_fkie::LoadLaunch >::value();
  }
  static const char* value(const ::multimaster_msgs_fkie::LoadLaunchRequest&)
  {
    return value();
  }
};

// service_traits::MD5Sum< ::multimaster_msgs_fkie::LoadLaunchResponse> should match 
// service_traits::MD5Sum< ::multimaster_msgs_fkie::LoadLaunch > 
template<>
struct MD5Sum< ::multimaster_msgs_fkie::LoadLaunchResponse>
{
  static const char* value()
  {
    return MD5Sum< ::multimaster_msgs_fkie::LoadLaunch >::value();
  }
  static const char* value(const ::multimaster_msgs_fkie::LoadLaunchResponse&)
  {
    return value();
  }
};

// service_traits::DataType< ::multimaster_msgs_fkie::LoadLaunchResponse> should match 
// service_traits::DataType< ::multimaster_msgs_fkie::LoadLaunch > 
template<>
struct DataType< ::multimaster_msgs_fkie::LoadLaunchResponse>
{
  static const char* value()
  {
    return DataType< ::multimaster_msgs_fkie::LoadLaunch >::value();
  }
  static const char* value(const ::multimaster_msgs_fkie::LoadLaunchResponse&)
  {
    return value();
  }
};

} // namespace service_traits
} // namespace ros

#endif // MULTIMASTER_MSGS_FKIE_MESSAGE_LOADLAUNCH_H
