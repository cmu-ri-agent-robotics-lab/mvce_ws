; Auto-generated. Do not edit!


(cl:in-package distributed_mvee-msg)


;//! \htmlinclude AllData.msg.html

(cl:defclass <AllData> (roslisp-msg-protocol:ros-message)
  ((receiver_ID
    :reader receiver_ID
    :initarg :receiver_ID
    :type cl:string
    :initform "")
   (sender_ID
    :reader sender_ID
    :initarg :sender_ID
    :type cl:string
    :initform "")
   (leader
    :reader leader
    :initarg :leader
    :type cl:fixnum
    :initform 0)
   (hop
    :reader hop
    :initarg :hop
    :type cl:fixnum
    :initform 0)
   (set_ID
    :reader set_ID
    :initarg :set_ID
    :type (cl:vector cl:fixnum)
   :initform (cl:make-array 0 :element-type 'cl:fixnum :initial-element 0))
   (set_x
    :reader set_x
    :initarg :set_x
    :type (cl:vector cl:float)
   :initform (cl:make-array 0 :element-type 'cl:float :initial-element 0.0))
   (set_y
    :reader set_y
    :initarg :set_y
    :type (cl:vector cl:float)
   :initform (cl:make-array 0 :element-type 'cl:float :initial-element 0.0))
   (time
    :reader time
    :initarg :time
    :type (cl:vector cl:float)
   :initform (cl:make-array 0 :element-type 'cl:float :initial-element 0.0))
   (priority
    :reader priority
    :initarg :priority
    :type cl:boolean
    :initform cl:nil))
)

(cl:defclass AllData (<AllData>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <AllData>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'AllData)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name distributed_mvee-msg:<AllData> is deprecated: use distributed_mvee-msg:AllData instead.")))

(cl:ensure-generic-function 'receiver_ID-val :lambda-list '(m))
(cl:defmethod receiver_ID-val ((m <AllData>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader distributed_mvee-msg:receiver_ID-val is deprecated.  Use distributed_mvee-msg:receiver_ID instead.")
  (receiver_ID m))

(cl:ensure-generic-function 'sender_ID-val :lambda-list '(m))
(cl:defmethod sender_ID-val ((m <AllData>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader distributed_mvee-msg:sender_ID-val is deprecated.  Use distributed_mvee-msg:sender_ID instead.")
  (sender_ID m))

(cl:ensure-generic-function 'leader-val :lambda-list '(m))
(cl:defmethod leader-val ((m <AllData>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader distributed_mvee-msg:leader-val is deprecated.  Use distributed_mvee-msg:leader instead.")
  (leader m))

(cl:ensure-generic-function 'hop-val :lambda-list '(m))
(cl:defmethod hop-val ((m <AllData>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader distributed_mvee-msg:hop-val is deprecated.  Use distributed_mvee-msg:hop instead.")
  (hop m))

(cl:ensure-generic-function 'set_ID-val :lambda-list '(m))
(cl:defmethod set_ID-val ((m <AllData>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader distributed_mvee-msg:set_ID-val is deprecated.  Use distributed_mvee-msg:set_ID instead.")
  (set_ID m))

(cl:ensure-generic-function 'set_x-val :lambda-list '(m))
(cl:defmethod set_x-val ((m <AllData>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader distributed_mvee-msg:set_x-val is deprecated.  Use distributed_mvee-msg:set_x instead.")
  (set_x m))

(cl:ensure-generic-function 'set_y-val :lambda-list '(m))
(cl:defmethod set_y-val ((m <AllData>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader distributed_mvee-msg:set_y-val is deprecated.  Use distributed_mvee-msg:set_y instead.")
  (set_y m))

(cl:ensure-generic-function 'time-val :lambda-list '(m))
(cl:defmethod time-val ((m <AllData>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader distributed_mvee-msg:time-val is deprecated.  Use distributed_mvee-msg:time instead.")
  (time m))

(cl:ensure-generic-function 'priority-val :lambda-list '(m))
(cl:defmethod priority-val ((m <AllData>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader distributed_mvee-msg:priority-val is deprecated.  Use distributed_mvee-msg:priority instead.")
  (priority m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <AllData>) ostream)
  "Serializes a message object of type '<AllData>"
  (cl:let ((__ros_str_len (cl:length (cl:slot-value msg 'receiver_ID))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_str_len) ostream))
  (cl:map cl:nil #'(cl:lambda (c) (cl:write-byte (cl:char-code c) ostream)) (cl:slot-value msg 'receiver_ID))
  (cl:let ((__ros_str_len (cl:length (cl:slot-value msg 'sender_ID))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_str_len) ostream))
  (cl:map cl:nil #'(cl:lambda (c) (cl:write-byte (cl:char-code c) ostream)) (cl:slot-value msg 'sender_ID))
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'leader)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'hop)) ostream)
  (cl:let ((__ros_arr_len (cl:length (cl:slot-value msg 'set_ID))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_arr_len) ostream))
  (cl:map cl:nil #'(cl:lambda (ele) (cl:let* ((signed ele) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 65536) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    ))
   (cl:slot-value msg 'set_ID))
  (cl:let ((__ros_arr_len (cl:length (cl:slot-value msg 'set_x))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_arr_len) ostream))
  (cl:map cl:nil #'(cl:lambda (ele) (cl:let ((bits (roslisp-utils:encode-double-float-bits ele)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream)))
   (cl:slot-value msg 'set_x))
  (cl:let ((__ros_arr_len (cl:length (cl:slot-value msg 'set_y))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_arr_len) ostream))
  (cl:map cl:nil #'(cl:lambda (ele) (cl:let ((bits (roslisp-utils:encode-double-float-bits ele)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream)))
   (cl:slot-value msg 'set_y))
  (cl:let ((__ros_arr_len (cl:length (cl:slot-value msg 'time))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_arr_len) ostream))
  (cl:map cl:nil #'(cl:lambda (ele) (cl:let ((bits (roslisp-utils:encode-double-float-bits ele)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream)))
   (cl:slot-value msg 'time))
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'priority) 1 0)) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <AllData>) istream)
  "Deserializes a message object of type '<AllData>"
    (cl:let ((__ros_str_len 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'receiver_ID) (cl:make-string __ros_str_len))
      (cl:dotimes (__ros_str_idx __ros_str_len msg)
        (cl:setf (cl:char (cl:slot-value msg 'receiver_ID) __ros_str_idx) (cl:code-char (cl:read-byte istream)))))
    (cl:let ((__ros_str_len 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'sender_ID) (cl:make-string __ros_str_len))
      (cl:dotimes (__ros_str_idx __ros_str_len msg)
        (cl:setf (cl:char (cl:slot-value msg 'sender_ID) __ros_str_idx) (cl:code-char (cl:read-byte istream)))))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'leader)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'hop)) (cl:read-byte istream))
  (cl:let ((__ros_arr_len 0))
    (cl:setf (cl:ldb (cl:byte 8 0) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) __ros_arr_len) (cl:read-byte istream))
  (cl:setf (cl:slot-value msg 'set_ID) (cl:make-array __ros_arr_len))
  (cl:let ((vals (cl:slot-value msg 'set_ID)))
    (cl:dotimes (i __ros_arr_len)
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:aref vals i) (cl:if (cl:< unsigned 32768) unsigned (cl:- unsigned 65536)))))))
  (cl:let ((__ros_arr_len 0))
    (cl:setf (cl:ldb (cl:byte 8 0) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) __ros_arr_len) (cl:read-byte istream))
  (cl:setf (cl:slot-value msg 'set_x) (cl:make-array __ros_arr_len))
  (cl:let ((vals (cl:slot-value msg 'set_x)))
    (cl:dotimes (i __ros_arr_len)
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:aref vals i) (roslisp-utils:decode-double-float-bits bits))))))
  (cl:let ((__ros_arr_len 0))
    (cl:setf (cl:ldb (cl:byte 8 0) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) __ros_arr_len) (cl:read-byte istream))
  (cl:setf (cl:slot-value msg 'set_y) (cl:make-array __ros_arr_len))
  (cl:let ((vals (cl:slot-value msg 'set_y)))
    (cl:dotimes (i __ros_arr_len)
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:aref vals i) (roslisp-utils:decode-double-float-bits bits))))))
  (cl:let ((__ros_arr_len 0))
    (cl:setf (cl:ldb (cl:byte 8 0) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) __ros_arr_len) (cl:read-byte istream))
  (cl:setf (cl:slot-value msg 'time) (cl:make-array __ros_arr_len))
  (cl:let ((vals (cl:slot-value msg 'time)))
    (cl:dotimes (i __ros_arr_len)
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:aref vals i) (roslisp-utils:decode-double-float-bits bits))))))
    (cl:setf (cl:slot-value msg 'priority) (cl:not (cl:zerop (cl:read-byte istream))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<AllData>)))
  "Returns string type for a message object of type '<AllData>"
  "distributed_mvee/AllData")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'AllData)))
  "Returns string type for a message object of type 'AllData"
  "distributed_mvee/AllData")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<AllData>)))
  "Returns md5sum for a message object of type '<AllData>"
  "44d76d268588843cbe4832d245a1c609")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'AllData)))
  "Returns md5sum for a message object of type 'AllData"
  "44d76d268588843cbe4832d245a1c609")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<AllData>)))
  "Returns full string definition for message of type '<AllData>"
  (cl:format cl:nil "string receiver_ID~%string sender_ID~%uint8 leader~%uint8 hop~%int16[] set_ID~%float64[] set_x~%float64[] set_y~%float64[] time~%bool priority~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'AllData)))
  "Returns full string definition for message of type 'AllData"
  (cl:format cl:nil "string receiver_ID~%string sender_ID~%uint8 leader~%uint8 hop~%int16[] set_ID~%float64[] set_x~%float64[] set_y~%float64[] time~%bool priority~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <AllData>))
  (cl:+ 0
     4 (cl:length (cl:slot-value msg 'receiver_ID))
     4 (cl:length (cl:slot-value msg 'sender_ID))
     1
     1
     4 (cl:reduce #'cl:+ (cl:slot-value msg 'set_ID) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ 2)))
     4 (cl:reduce #'cl:+ (cl:slot-value msg 'set_x) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ 8)))
     4 (cl:reduce #'cl:+ (cl:slot-value msg 'set_y) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ 8)))
     4 (cl:reduce #'cl:+ (cl:slot-value msg 'time) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ 8)))
     1
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <AllData>))
  "Converts a ROS message object to a list"
  (cl:list 'AllData
    (cl:cons ':receiver_ID (receiver_ID msg))
    (cl:cons ':sender_ID (sender_ID msg))
    (cl:cons ':leader (leader msg))
    (cl:cons ':hop (hop msg))
    (cl:cons ':set_ID (set_ID msg))
    (cl:cons ':set_x (set_x msg))
    (cl:cons ':set_y (set_y msg))
    (cl:cons ':time (time msg))
    (cl:cons ':priority (priority msg))
))
