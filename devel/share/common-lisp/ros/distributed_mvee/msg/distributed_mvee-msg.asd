
(cl:in-package :asdf)

(defsystem "distributed_mvee-msg"
  :depends-on (:roslisp-msg-protocol :roslisp-utils )
  :components ((:file "_package")
    (:file "ExcludeData" :depends-on ("_package_ExcludeData"))
    (:file "_package_ExcludeData" :depends-on ("_package"))
    (:file "AllData" :depends-on ("_package_AllData"))
    (:file "_package_AllData" :depends-on ("_package"))
  ))