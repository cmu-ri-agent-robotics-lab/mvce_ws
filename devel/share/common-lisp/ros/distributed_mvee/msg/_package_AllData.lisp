(cl:in-package distributed_mvee-msg)
(cl:export '(RECEIVER_ID-VAL
          RECEIVER_ID
          SENDER_ID-VAL
          SENDER_ID
          LEADER-VAL
          LEADER
          HOP-VAL
          HOP
          SET_ID-VAL
          SET_ID
          SET_X-VAL
          SET_X
          SET_Y-VAL
          SET_Y
          TIME-VAL
          TIME
          PRIORITY-VAL
          PRIORITY
))