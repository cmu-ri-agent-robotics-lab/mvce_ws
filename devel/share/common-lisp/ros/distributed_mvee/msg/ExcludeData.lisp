; Auto-generated. Do not edit!


(cl:in-package distributed_mvee-msg)


;//! \htmlinclude ExcludeData.msg.html

(cl:defclass <ExcludeData> (roslisp-msg-protocol:ros-message)
  ((exclude_ID
    :reader exclude_ID
    :initarg :exclude_ID
    :type (cl:vector cl:fixnum)
   :initform (cl:make-array 0 :element-type 'cl:fixnum :initial-element 0))
   (time
    :reader time
    :initarg :time
    :type (cl:vector cl:float)
   :initform (cl:make-array 0 :element-type 'cl:float :initial-element 0.0)))
)

(cl:defclass ExcludeData (<ExcludeData>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <ExcludeData>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'ExcludeData)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name distributed_mvee-msg:<ExcludeData> is deprecated: use distributed_mvee-msg:ExcludeData instead.")))

(cl:ensure-generic-function 'exclude_ID-val :lambda-list '(m))
(cl:defmethod exclude_ID-val ((m <ExcludeData>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader distributed_mvee-msg:exclude_ID-val is deprecated.  Use distributed_mvee-msg:exclude_ID instead.")
  (exclude_ID m))

(cl:ensure-generic-function 'time-val :lambda-list '(m))
(cl:defmethod time-val ((m <ExcludeData>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader distributed_mvee-msg:time-val is deprecated.  Use distributed_mvee-msg:time instead.")
  (time m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <ExcludeData>) ostream)
  "Serializes a message object of type '<ExcludeData>"
  (cl:let ((__ros_arr_len (cl:length (cl:slot-value msg 'exclude_ID))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_arr_len) ostream))
  (cl:map cl:nil #'(cl:lambda (ele) (cl:let* ((signed ele) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 65536) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    ))
   (cl:slot-value msg 'exclude_ID))
  (cl:let ((__ros_arr_len (cl:length (cl:slot-value msg 'time))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_arr_len) ostream))
  (cl:map cl:nil #'(cl:lambda (ele) (cl:let ((bits (roslisp-utils:encode-double-float-bits ele)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream)))
   (cl:slot-value msg 'time))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <ExcludeData>) istream)
  "Deserializes a message object of type '<ExcludeData>"
  (cl:let ((__ros_arr_len 0))
    (cl:setf (cl:ldb (cl:byte 8 0) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) __ros_arr_len) (cl:read-byte istream))
  (cl:setf (cl:slot-value msg 'exclude_ID) (cl:make-array __ros_arr_len))
  (cl:let ((vals (cl:slot-value msg 'exclude_ID)))
    (cl:dotimes (i __ros_arr_len)
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:aref vals i) (cl:if (cl:< unsigned 32768) unsigned (cl:- unsigned 65536)))))))
  (cl:let ((__ros_arr_len 0))
    (cl:setf (cl:ldb (cl:byte 8 0) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) __ros_arr_len) (cl:read-byte istream))
  (cl:setf (cl:slot-value msg 'time) (cl:make-array __ros_arr_len))
  (cl:let ((vals (cl:slot-value msg 'time)))
    (cl:dotimes (i __ros_arr_len)
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:aref vals i) (roslisp-utils:decode-double-float-bits bits))))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<ExcludeData>)))
  "Returns string type for a message object of type '<ExcludeData>"
  "distributed_mvee/ExcludeData")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'ExcludeData)))
  "Returns string type for a message object of type 'ExcludeData"
  "distributed_mvee/ExcludeData")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<ExcludeData>)))
  "Returns md5sum for a message object of type '<ExcludeData>"
  "76e43fc19d47e7228ed594ebee026bb1")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'ExcludeData)))
  "Returns md5sum for a message object of type 'ExcludeData"
  "76e43fc19d47e7228ed594ebee026bb1")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<ExcludeData>)))
  "Returns full string definition for message of type '<ExcludeData>"
  (cl:format cl:nil "int16[] exclude_ID~%float64[] time~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'ExcludeData)))
  "Returns full string definition for message of type 'ExcludeData"
  (cl:format cl:nil "int16[] exclude_ID~%float64[] time~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <ExcludeData>))
  (cl:+ 0
     4 (cl:reduce #'cl:+ (cl:slot-value msg 'exclude_ID) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ 2)))
     4 (cl:reduce #'cl:+ (cl:slot-value msg 'time) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ 8)))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <ExcludeData>))
  "Converts a ROS message object to a list"
  (cl:list 'ExcludeData
    (cl:cons ':exclude_ID (exclude_ID msg))
    (cl:cons ':time (time msg))
))
