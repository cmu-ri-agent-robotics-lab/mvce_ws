# generated from genmsg/cmake/pkg-genmsg.cmake.em

message(STATUS "distributed_mvee: 2 messages, 0 services")

set(MSG_I_FLAGS "-Idistributed_mvee:/home/turtlebot/ros/workspaces/hydro/mvce_ws/src/distributed_mvee/msg;-Istd_msgs:/opt/ros/hydro/share/std_msgs/cmake/../msg")

# Find all generators
find_package(gencpp REQUIRED)
find_package(genlisp REQUIRED)
find_package(genpy REQUIRED)

add_custom_target(distributed_mvee_generate_messages ALL)

#
#  langs = gencpp;genlisp;genpy
#

### Section generating for lang: gencpp
### Generating Messages
_generate_msg_cpp(distributed_mvee
  "/home/turtlebot/ros/workspaces/hydro/mvce_ws/src/distributed_mvee/msg/AllData.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/distributed_mvee
)
_generate_msg_cpp(distributed_mvee
  "/home/turtlebot/ros/workspaces/hydro/mvce_ws/src/distributed_mvee/msg/ExcludeData.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/distributed_mvee
)

### Generating Services

### Generating Module File
_generate_module_cpp(distributed_mvee
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/distributed_mvee
  "${ALL_GEN_OUTPUT_FILES_cpp}"
)

add_custom_target(distributed_mvee_generate_messages_cpp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_cpp}
)
add_dependencies(distributed_mvee_generate_messages distributed_mvee_generate_messages_cpp)

# target for backward compatibility
add_custom_target(distributed_mvee_gencpp)
add_dependencies(distributed_mvee_gencpp distributed_mvee_generate_messages_cpp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS distributed_mvee_generate_messages_cpp)

### Section generating for lang: genlisp
### Generating Messages
_generate_msg_lisp(distributed_mvee
  "/home/turtlebot/ros/workspaces/hydro/mvce_ws/src/distributed_mvee/msg/AllData.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/distributed_mvee
)
_generate_msg_lisp(distributed_mvee
  "/home/turtlebot/ros/workspaces/hydro/mvce_ws/src/distributed_mvee/msg/ExcludeData.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/distributed_mvee
)

### Generating Services

### Generating Module File
_generate_module_lisp(distributed_mvee
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/distributed_mvee
  "${ALL_GEN_OUTPUT_FILES_lisp}"
)

add_custom_target(distributed_mvee_generate_messages_lisp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_lisp}
)
add_dependencies(distributed_mvee_generate_messages distributed_mvee_generate_messages_lisp)

# target for backward compatibility
add_custom_target(distributed_mvee_genlisp)
add_dependencies(distributed_mvee_genlisp distributed_mvee_generate_messages_lisp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS distributed_mvee_generate_messages_lisp)

### Section generating for lang: genpy
### Generating Messages
_generate_msg_py(distributed_mvee
  "/home/turtlebot/ros/workspaces/hydro/mvce_ws/src/distributed_mvee/msg/AllData.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/distributed_mvee
)
_generate_msg_py(distributed_mvee
  "/home/turtlebot/ros/workspaces/hydro/mvce_ws/src/distributed_mvee/msg/ExcludeData.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/distributed_mvee
)

### Generating Services

### Generating Module File
_generate_module_py(distributed_mvee
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/distributed_mvee
  "${ALL_GEN_OUTPUT_FILES_py}"
)

add_custom_target(distributed_mvee_generate_messages_py
  DEPENDS ${ALL_GEN_OUTPUT_FILES_py}
)
add_dependencies(distributed_mvee_generate_messages distributed_mvee_generate_messages_py)

# target for backward compatibility
add_custom_target(distributed_mvee_genpy)
add_dependencies(distributed_mvee_genpy distributed_mvee_generate_messages_py)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS distributed_mvee_generate_messages_py)



if(gencpp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/distributed_mvee)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/distributed_mvee
    DESTINATION ${gencpp_INSTALL_DIR}
  )
endif()
add_dependencies(distributed_mvee_generate_messages_cpp std_msgs_generate_messages_cpp)

if(genlisp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/distributed_mvee)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/distributed_mvee
    DESTINATION ${genlisp_INSTALL_DIR}
  )
endif()
add_dependencies(distributed_mvee_generate_messages_lisp std_msgs_generate_messages_lisp)

if(genpy_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/distributed_mvee)
  install(CODE "execute_process(COMMAND \"/usr/bin/python\" -m compileall \"${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/distributed_mvee\")")
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/distributed_mvee
    DESTINATION ${genpy_INSTALL_DIR}
  )
endif()
add_dependencies(distributed_mvee_generate_messages_py std_msgs_generate_messages_py)
