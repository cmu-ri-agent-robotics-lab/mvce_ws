# CMake generated Testfile for 
# Source directory: /home/turtlebot/ros/workspaces/hydro/mvce_ws/src
# Build directory: /home/turtlebot/ros/workspaces/hydro/mvce_ws/build
# 
# This file includes the relevent testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
SUBDIRS(gtest)
SUBDIRS(distributed_mvee)
SUBDIRS(led_node)
SUBDIRS(mvee_turtlebot)
SUBDIRS(mvee_workstation)
SUBDIRS(intraface)
SUBDIRS(random_walk_bump)
SUBDIRS(random_walk_kinect)
