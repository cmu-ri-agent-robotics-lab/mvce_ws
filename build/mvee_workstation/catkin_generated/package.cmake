set(_CATKIN_CURRENT_PACKAGE "mvee_workstation")
set(mvee_workstation_MAINTAINER "turtlebot <turtlebot@todo.todo>")
set(mvee_workstation_DEPRECATED "")
set(mvee_workstation_VERSION "0.0.0")
set(mvee_workstation_BUILD_DEPENDS "rospy")
set(mvee_workstation_RUN_DEPENDS "rospy")
set(mvee_workstation_BUILDTOOL_DEPENDS "catkin")