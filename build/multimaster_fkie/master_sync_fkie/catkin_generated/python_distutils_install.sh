#!/bin/sh -x

if [ -n "$DESTDIR" ] ; then
    case $DESTDIR in
        /*) # ok
            ;;
        *)
            /bin/echo "DESTDIR argument must be absolute... "
            /bin/echo "otherwise python's distutils will bork things."
            exit 1
    esac
    DESTDIR_ARG="--root=$DESTDIR"
fi

cd "/home/turtlebot/ros/workspaces/hydro/mvce_ws/src/multimaster_fkie/master_sync_fkie"

# Note that PYTHONPATH is pulled from the environment to support installing
# into one location when some dependencies were installed in another
# location, #123.
/usr/bin/env \
    PYTHONPATH="/home/turtlebot/ros/workspaces/hydro/mvce_ws/install/lib/python2.7/dist-packages:/home/turtlebot/ros/workspaces/hydro/mvce_ws/build/lib/python2.7/dist-packages:$PYTHONPATH" \
    CATKIN_BINARY_DIR="/home/turtlebot/ros/workspaces/hydro/mvce_ws/build" \
    "/usr/bin/python" \
    "/home/turtlebot/ros/workspaces/hydro/mvce_ws/src/multimaster_fkie/master_sync_fkie/setup.py" \
    build --build-base "/home/turtlebot/ros/workspaces/hydro/mvce_ws/build/multimaster_fkie/master_sync_fkie" \
    install \
    $DESTDIR_ARG \
    --install-layout=deb --prefix="/home/turtlebot/ros/workspaces/hydro/mvce_ws/install" --install-scripts="/home/turtlebot/ros/workspaces/hydro/mvce_ws/install/bin"
