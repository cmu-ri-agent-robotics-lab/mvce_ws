# generated from genmsg/cmake/pkg-genmsg.cmake.em

message(STATUS "multimaster_msgs_fkie: 8 messages, 6 services")

set(MSG_I_FLAGS "-Imultimaster_msgs_fkie:/home/turtlebot/ros/workspaces/hydro/mvce_ws/src/multimaster_fkie/multimaster_msgs_fkie/msg;-Istd_msgs:/opt/ros/hydro/share/std_msgs/cmake/../msg")

# Find all generators
find_package(gencpp REQUIRED)
find_package(genlisp REQUIRED)
find_package(genpy REQUIRED)

add_custom_target(multimaster_msgs_fkie_generate_messages ALL)

#
#  langs = gencpp;genlisp;genpy
#

### Section generating for lang: gencpp
### Generating Messages
_generate_msg_cpp(multimaster_msgs_fkie
  "/home/turtlebot/ros/workspaces/hydro/mvce_ws/src/multimaster_fkie/multimaster_msgs_fkie/msg/SyncMasterInfo.msg"
  "${MSG_I_FLAGS}"
  "/home/turtlebot/ros/workspaces/hydro/mvce_ws/src/multimaster_fkie/multimaster_msgs_fkie/msg/SyncTopicInfo.msg;/home/turtlebot/ros/workspaces/hydro/mvce_ws/src/multimaster_fkie/multimaster_msgs_fkie/msg/SyncServiceInfo.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/multimaster_msgs_fkie
)
_generate_msg_cpp(multimaster_msgs_fkie
  "/home/turtlebot/ros/workspaces/hydro/mvce_ws/src/multimaster_fkie/multimaster_msgs_fkie/msg/Capability.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/multimaster_msgs_fkie
)
_generate_msg_cpp(multimaster_msgs_fkie
  "/home/turtlebot/ros/workspaces/hydro/mvce_ws/src/multimaster_fkie/multimaster_msgs_fkie/msg/LinkStatesStamped.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/hydro/share/std_msgs/cmake/../msg/Header.msg;/home/turtlebot/ros/workspaces/hydro/mvce_ws/src/multimaster_fkie/multimaster_msgs_fkie/msg/LinkState.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/multimaster_msgs_fkie
)
_generate_msg_cpp(multimaster_msgs_fkie
  "/home/turtlebot/ros/workspaces/hydro/mvce_ws/src/multimaster_fkie/multimaster_msgs_fkie/msg/SyncServiceInfo.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/multimaster_msgs_fkie
)
_generate_msg_cpp(multimaster_msgs_fkie
  "/home/turtlebot/ros/workspaces/hydro/mvce_ws/src/multimaster_fkie/multimaster_msgs_fkie/msg/SyncTopicInfo.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/multimaster_msgs_fkie
)
_generate_msg_cpp(multimaster_msgs_fkie
  "/home/turtlebot/ros/workspaces/hydro/mvce_ws/src/multimaster_fkie/multimaster_msgs_fkie/msg/MasterState.msg"
  "${MSG_I_FLAGS}"
  "/home/turtlebot/ros/workspaces/hydro/mvce_ws/src/multimaster_fkie/multimaster_msgs_fkie/msg/ROSMaster.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/multimaster_msgs_fkie
)
_generate_msg_cpp(multimaster_msgs_fkie
  "/home/turtlebot/ros/workspaces/hydro/mvce_ws/src/multimaster_fkie/multimaster_msgs_fkie/msg/ROSMaster.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/multimaster_msgs_fkie
)
_generate_msg_cpp(multimaster_msgs_fkie
  "/home/turtlebot/ros/workspaces/hydro/mvce_ws/src/multimaster_fkie/multimaster_msgs_fkie/msg/LinkState.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/multimaster_msgs_fkie
)

### Generating Services
_generate_srv_cpp(multimaster_msgs_fkie
  "/home/turtlebot/ros/workspaces/hydro/mvce_ws/src/multimaster_fkie/multimaster_msgs_fkie/srv/ListNodes.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/multimaster_msgs_fkie
)
_generate_srv_cpp(multimaster_msgs_fkie
  "/home/turtlebot/ros/workspaces/hydro/mvce_ws/src/multimaster_fkie/multimaster_msgs_fkie/srv/DiscoverMasters.srv"
  "${MSG_I_FLAGS}"
  "/home/turtlebot/ros/workspaces/hydro/mvce_ws/src/multimaster_fkie/multimaster_msgs_fkie/msg/ROSMaster.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/multimaster_msgs_fkie
)
_generate_srv_cpp(multimaster_msgs_fkie
  "/home/turtlebot/ros/workspaces/hydro/mvce_ws/src/multimaster_fkie/multimaster_msgs_fkie/srv/ListDescription.srv"
  "${MSG_I_FLAGS}"
  "/home/turtlebot/ros/workspaces/hydro/mvce_ws/src/multimaster_fkie/multimaster_msgs_fkie/msg/Capability.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/multimaster_msgs_fkie
)
_generate_srv_cpp(multimaster_msgs_fkie
  "/home/turtlebot/ros/workspaces/hydro/mvce_ws/src/multimaster_fkie/multimaster_msgs_fkie/srv/LoadLaunch.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/multimaster_msgs_fkie
)
_generate_srv_cpp(multimaster_msgs_fkie
  "/home/turtlebot/ros/workspaces/hydro/mvce_ws/src/multimaster_fkie/multimaster_msgs_fkie/srv/GetSyncInfo.srv"
  "${MSG_I_FLAGS}"
  "/home/turtlebot/ros/workspaces/hydro/mvce_ws/src/multimaster_fkie/multimaster_msgs_fkie/msg/SyncTopicInfo.msg;/home/turtlebot/ros/workspaces/hydro/mvce_ws/src/multimaster_fkie/multimaster_msgs_fkie/msg/SyncMasterInfo.msg;/home/turtlebot/ros/workspaces/hydro/mvce_ws/src/multimaster_fkie/multimaster_msgs_fkie/msg/SyncServiceInfo.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/multimaster_msgs_fkie
)
_generate_srv_cpp(multimaster_msgs_fkie
  "/home/turtlebot/ros/workspaces/hydro/mvce_ws/src/multimaster_fkie/multimaster_msgs_fkie/srv/Task.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/multimaster_msgs_fkie
)

### Generating Module File
_generate_module_cpp(multimaster_msgs_fkie
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/multimaster_msgs_fkie
  "${ALL_GEN_OUTPUT_FILES_cpp}"
)

add_custom_target(multimaster_msgs_fkie_generate_messages_cpp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_cpp}
)
add_dependencies(multimaster_msgs_fkie_generate_messages multimaster_msgs_fkie_generate_messages_cpp)

# target for backward compatibility
add_custom_target(multimaster_msgs_fkie_gencpp)
add_dependencies(multimaster_msgs_fkie_gencpp multimaster_msgs_fkie_generate_messages_cpp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS multimaster_msgs_fkie_generate_messages_cpp)

### Section generating for lang: genlisp
### Generating Messages
_generate_msg_lisp(multimaster_msgs_fkie
  "/home/turtlebot/ros/workspaces/hydro/mvce_ws/src/multimaster_fkie/multimaster_msgs_fkie/msg/SyncMasterInfo.msg"
  "${MSG_I_FLAGS}"
  "/home/turtlebot/ros/workspaces/hydro/mvce_ws/src/multimaster_fkie/multimaster_msgs_fkie/msg/SyncTopicInfo.msg;/home/turtlebot/ros/workspaces/hydro/mvce_ws/src/multimaster_fkie/multimaster_msgs_fkie/msg/SyncServiceInfo.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/multimaster_msgs_fkie
)
_generate_msg_lisp(multimaster_msgs_fkie
  "/home/turtlebot/ros/workspaces/hydro/mvce_ws/src/multimaster_fkie/multimaster_msgs_fkie/msg/Capability.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/multimaster_msgs_fkie
)
_generate_msg_lisp(multimaster_msgs_fkie
  "/home/turtlebot/ros/workspaces/hydro/mvce_ws/src/multimaster_fkie/multimaster_msgs_fkie/msg/LinkStatesStamped.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/hydro/share/std_msgs/cmake/../msg/Header.msg;/home/turtlebot/ros/workspaces/hydro/mvce_ws/src/multimaster_fkie/multimaster_msgs_fkie/msg/LinkState.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/multimaster_msgs_fkie
)
_generate_msg_lisp(multimaster_msgs_fkie
  "/home/turtlebot/ros/workspaces/hydro/mvce_ws/src/multimaster_fkie/multimaster_msgs_fkie/msg/SyncServiceInfo.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/multimaster_msgs_fkie
)
_generate_msg_lisp(multimaster_msgs_fkie
  "/home/turtlebot/ros/workspaces/hydro/mvce_ws/src/multimaster_fkie/multimaster_msgs_fkie/msg/SyncTopicInfo.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/multimaster_msgs_fkie
)
_generate_msg_lisp(multimaster_msgs_fkie
  "/home/turtlebot/ros/workspaces/hydro/mvce_ws/src/multimaster_fkie/multimaster_msgs_fkie/msg/MasterState.msg"
  "${MSG_I_FLAGS}"
  "/home/turtlebot/ros/workspaces/hydro/mvce_ws/src/multimaster_fkie/multimaster_msgs_fkie/msg/ROSMaster.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/multimaster_msgs_fkie
)
_generate_msg_lisp(multimaster_msgs_fkie
  "/home/turtlebot/ros/workspaces/hydro/mvce_ws/src/multimaster_fkie/multimaster_msgs_fkie/msg/ROSMaster.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/multimaster_msgs_fkie
)
_generate_msg_lisp(multimaster_msgs_fkie
  "/home/turtlebot/ros/workspaces/hydro/mvce_ws/src/multimaster_fkie/multimaster_msgs_fkie/msg/LinkState.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/multimaster_msgs_fkie
)

### Generating Services
_generate_srv_lisp(multimaster_msgs_fkie
  "/home/turtlebot/ros/workspaces/hydro/mvce_ws/src/multimaster_fkie/multimaster_msgs_fkie/srv/ListNodes.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/multimaster_msgs_fkie
)
_generate_srv_lisp(multimaster_msgs_fkie
  "/home/turtlebot/ros/workspaces/hydro/mvce_ws/src/multimaster_fkie/multimaster_msgs_fkie/srv/DiscoverMasters.srv"
  "${MSG_I_FLAGS}"
  "/home/turtlebot/ros/workspaces/hydro/mvce_ws/src/multimaster_fkie/multimaster_msgs_fkie/msg/ROSMaster.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/multimaster_msgs_fkie
)
_generate_srv_lisp(multimaster_msgs_fkie
  "/home/turtlebot/ros/workspaces/hydro/mvce_ws/src/multimaster_fkie/multimaster_msgs_fkie/srv/ListDescription.srv"
  "${MSG_I_FLAGS}"
  "/home/turtlebot/ros/workspaces/hydro/mvce_ws/src/multimaster_fkie/multimaster_msgs_fkie/msg/Capability.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/multimaster_msgs_fkie
)
_generate_srv_lisp(multimaster_msgs_fkie
  "/home/turtlebot/ros/workspaces/hydro/mvce_ws/src/multimaster_fkie/multimaster_msgs_fkie/srv/LoadLaunch.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/multimaster_msgs_fkie
)
_generate_srv_lisp(multimaster_msgs_fkie
  "/home/turtlebot/ros/workspaces/hydro/mvce_ws/src/multimaster_fkie/multimaster_msgs_fkie/srv/GetSyncInfo.srv"
  "${MSG_I_FLAGS}"
  "/home/turtlebot/ros/workspaces/hydro/mvce_ws/src/multimaster_fkie/multimaster_msgs_fkie/msg/SyncTopicInfo.msg;/home/turtlebot/ros/workspaces/hydro/mvce_ws/src/multimaster_fkie/multimaster_msgs_fkie/msg/SyncMasterInfo.msg;/home/turtlebot/ros/workspaces/hydro/mvce_ws/src/multimaster_fkie/multimaster_msgs_fkie/msg/SyncServiceInfo.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/multimaster_msgs_fkie
)
_generate_srv_lisp(multimaster_msgs_fkie
  "/home/turtlebot/ros/workspaces/hydro/mvce_ws/src/multimaster_fkie/multimaster_msgs_fkie/srv/Task.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/multimaster_msgs_fkie
)

### Generating Module File
_generate_module_lisp(multimaster_msgs_fkie
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/multimaster_msgs_fkie
  "${ALL_GEN_OUTPUT_FILES_lisp}"
)

add_custom_target(multimaster_msgs_fkie_generate_messages_lisp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_lisp}
)
add_dependencies(multimaster_msgs_fkie_generate_messages multimaster_msgs_fkie_generate_messages_lisp)

# target for backward compatibility
add_custom_target(multimaster_msgs_fkie_genlisp)
add_dependencies(multimaster_msgs_fkie_genlisp multimaster_msgs_fkie_generate_messages_lisp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS multimaster_msgs_fkie_generate_messages_lisp)

### Section generating for lang: genpy
### Generating Messages
_generate_msg_py(multimaster_msgs_fkie
  "/home/turtlebot/ros/workspaces/hydro/mvce_ws/src/multimaster_fkie/multimaster_msgs_fkie/msg/SyncMasterInfo.msg"
  "${MSG_I_FLAGS}"
  "/home/turtlebot/ros/workspaces/hydro/mvce_ws/src/multimaster_fkie/multimaster_msgs_fkie/msg/SyncTopicInfo.msg;/home/turtlebot/ros/workspaces/hydro/mvce_ws/src/multimaster_fkie/multimaster_msgs_fkie/msg/SyncServiceInfo.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/multimaster_msgs_fkie
)
_generate_msg_py(multimaster_msgs_fkie
  "/home/turtlebot/ros/workspaces/hydro/mvce_ws/src/multimaster_fkie/multimaster_msgs_fkie/msg/Capability.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/multimaster_msgs_fkie
)
_generate_msg_py(multimaster_msgs_fkie
  "/home/turtlebot/ros/workspaces/hydro/mvce_ws/src/multimaster_fkie/multimaster_msgs_fkie/msg/LinkStatesStamped.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/hydro/share/std_msgs/cmake/../msg/Header.msg;/home/turtlebot/ros/workspaces/hydro/mvce_ws/src/multimaster_fkie/multimaster_msgs_fkie/msg/LinkState.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/multimaster_msgs_fkie
)
_generate_msg_py(multimaster_msgs_fkie
  "/home/turtlebot/ros/workspaces/hydro/mvce_ws/src/multimaster_fkie/multimaster_msgs_fkie/msg/SyncServiceInfo.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/multimaster_msgs_fkie
)
_generate_msg_py(multimaster_msgs_fkie
  "/home/turtlebot/ros/workspaces/hydro/mvce_ws/src/multimaster_fkie/multimaster_msgs_fkie/msg/SyncTopicInfo.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/multimaster_msgs_fkie
)
_generate_msg_py(multimaster_msgs_fkie
  "/home/turtlebot/ros/workspaces/hydro/mvce_ws/src/multimaster_fkie/multimaster_msgs_fkie/msg/MasterState.msg"
  "${MSG_I_FLAGS}"
  "/home/turtlebot/ros/workspaces/hydro/mvce_ws/src/multimaster_fkie/multimaster_msgs_fkie/msg/ROSMaster.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/multimaster_msgs_fkie
)
_generate_msg_py(multimaster_msgs_fkie
  "/home/turtlebot/ros/workspaces/hydro/mvce_ws/src/multimaster_fkie/multimaster_msgs_fkie/msg/ROSMaster.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/multimaster_msgs_fkie
)
_generate_msg_py(multimaster_msgs_fkie
  "/home/turtlebot/ros/workspaces/hydro/mvce_ws/src/multimaster_fkie/multimaster_msgs_fkie/msg/LinkState.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/multimaster_msgs_fkie
)

### Generating Services
_generate_srv_py(multimaster_msgs_fkie
  "/home/turtlebot/ros/workspaces/hydro/mvce_ws/src/multimaster_fkie/multimaster_msgs_fkie/srv/ListNodes.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/multimaster_msgs_fkie
)
_generate_srv_py(multimaster_msgs_fkie
  "/home/turtlebot/ros/workspaces/hydro/mvce_ws/src/multimaster_fkie/multimaster_msgs_fkie/srv/DiscoverMasters.srv"
  "${MSG_I_FLAGS}"
  "/home/turtlebot/ros/workspaces/hydro/mvce_ws/src/multimaster_fkie/multimaster_msgs_fkie/msg/ROSMaster.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/multimaster_msgs_fkie
)
_generate_srv_py(multimaster_msgs_fkie
  "/home/turtlebot/ros/workspaces/hydro/mvce_ws/src/multimaster_fkie/multimaster_msgs_fkie/srv/ListDescription.srv"
  "${MSG_I_FLAGS}"
  "/home/turtlebot/ros/workspaces/hydro/mvce_ws/src/multimaster_fkie/multimaster_msgs_fkie/msg/Capability.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/multimaster_msgs_fkie
)
_generate_srv_py(multimaster_msgs_fkie
  "/home/turtlebot/ros/workspaces/hydro/mvce_ws/src/multimaster_fkie/multimaster_msgs_fkie/srv/LoadLaunch.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/multimaster_msgs_fkie
)
_generate_srv_py(multimaster_msgs_fkie
  "/home/turtlebot/ros/workspaces/hydro/mvce_ws/src/multimaster_fkie/multimaster_msgs_fkie/srv/GetSyncInfo.srv"
  "${MSG_I_FLAGS}"
  "/home/turtlebot/ros/workspaces/hydro/mvce_ws/src/multimaster_fkie/multimaster_msgs_fkie/msg/SyncTopicInfo.msg;/home/turtlebot/ros/workspaces/hydro/mvce_ws/src/multimaster_fkie/multimaster_msgs_fkie/msg/SyncMasterInfo.msg;/home/turtlebot/ros/workspaces/hydro/mvce_ws/src/multimaster_fkie/multimaster_msgs_fkie/msg/SyncServiceInfo.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/multimaster_msgs_fkie
)
_generate_srv_py(multimaster_msgs_fkie
  "/home/turtlebot/ros/workspaces/hydro/mvce_ws/src/multimaster_fkie/multimaster_msgs_fkie/srv/Task.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/multimaster_msgs_fkie
)

### Generating Module File
_generate_module_py(multimaster_msgs_fkie
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/multimaster_msgs_fkie
  "${ALL_GEN_OUTPUT_FILES_py}"
)

add_custom_target(multimaster_msgs_fkie_generate_messages_py
  DEPENDS ${ALL_GEN_OUTPUT_FILES_py}
)
add_dependencies(multimaster_msgs_fkie_generate_messages multimaster_msgs_fkie_generate_messages_py)

# target for backward compatibility
add_custom_target(multimaster_msgs_fkie_genpy)
add_dependencies(multimaster_msgs_fkie_genpy multimaster_msgs_fkie_generate_messages_py)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS multimaster_msgs_fkie_generate_messages_py)



if(gencpp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/multimaster_msgs_fkie)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/multimaster_msgs_fkie
    DESTINATION ${gencpp_INSTALL_DIR}
  )
endif()
add_dependencies(multimaster_msgs_fkie_generate_messages_cpp std_msgs_generate_messages_cpp)

if(genlisp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/multimaster_msgs_fkie)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/multimaster_msgs_fkie
    DESTINATION ${genlisp_INSTALL_DIR}
  )
endif()
add_dependencies(multimaster_msgs_fkie_generate_messages_lisp std_msgs_generate_messages_lisp)

if(genpy_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/multimaster_msgs_fkie)
  install(CODE "execute_process(COMMAND \"/usr/bin/python\" -m compileall \"${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/multimaster_msgs_fkie\")")
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/multimaster_msgs_fkie
    DESTINATION ${genpy_INSTALL_DIR}
  )
endif()
add_dependencies(multimaster_msgs_fkie_generate_messages_py std_msgs_generate_messages_py)
