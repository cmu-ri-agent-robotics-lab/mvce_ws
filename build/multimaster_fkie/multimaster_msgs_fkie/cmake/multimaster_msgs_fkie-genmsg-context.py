# generated from genmsg/cmake/pkg-genmsg.context.in

messages_str = "/home/turtlebot/ros/workspaces/hydro/mvce_ws/src/multimaster_fkie/multimaster_msgs_fkie/msg/Capability.msg;/home/turtlebot/ros/workspaces/hydro/mvce_ws/src/multimaster_fkie/multimaster_msgs_fkie/msg/LinkState.msg;/home/turtlebot/ros/workspaces/hydro/mvce_ws/src/multimaster_fkie/multimaster_msgs_fkie/msg/LinkStatesStamped.msg;/home/turtlebot/ros/workspaces/hydro/mvce_ws/src/multimaster_fkie/multimaster_msgs_fkie/msg/MasterState.msg;/home/turtlebot/ros/workspaces/hydro/mvce_ws/src/multimaster_fkie/multimaster_msgs_fkie/msg/ROSMaster.msg;/home/turtlebot/ros/workspaces/hydro/mvce_ws/src/multimaster_fkie/multimaster_msgs_fkie/msg/SyncMasterInfo.msg;/home/turtlebot/ros/workspaces/hydro/mvce_ws/src/multimaster_fkie/multimaster_msgs_fkie/msg/SyncServiceInfo.msg;/home/turtlebot/ros/workspaces/hydro/mvce_ws/src/multimaster_fkie/multimaster_msgs_fkie/msg/SyncTopicInfo.msg"
services_str = "/home/turtlebot/ros/workspaces/hydro/mvce_ws/src/multimaster_fkie/multimaster_msgs_fkie/srv/DiscoverMasters.srv;/home/turtlebot/ros/workspaces/hydro/mvce_ws/src/multimaster_fkie/multimaster_msgs_fkie/srv/GetSyncInfo.srv;/home/turtlebot/ros/workspaces/hydro/mvce_ws/src/multimaster_fkie/multimaster_msgs_fkie/srv/ListDescription.srv;/home/turtlebot/ros/workspaces/hydro/mvce_ws/src/multimaster_fkie/multimaster_msgs_fkie/srv/ListNodes.srv;/home/turtlebot/ros/workspaces/hydro/mvce_ws/src/multimaster_fkie/multimaster_msgs_fkie/srv/LoadLaunch.srv;/home/turtlebot/ros/workspaces/hydro/mvce_ws/src/multimaster_fkie/multimaster_msgs_fkie/srv/Task.srv"
pkg_name = "multimaster_msgs_fkie"
dependencies_str = "std_msgs"
langs = "gencpp;genlisp;genpy"
dep_include_paths_str = "multimaster_msgs_fkie;/home/turtlebot/ros/workspaces/hydro/mvce_ws/src/multimaster_fkie/multimaster_msgs_fkie/msg;std_msgs;/opt/ros/hydro/share/std_msgs/cmake/../msg"
PYTHON_EXECUTABLE = "/usr/bin/python"
package_has_static_sources = '' == 'TRUE'
