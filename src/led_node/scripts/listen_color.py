#!/usr/bin/python2.7
import rospy
from std_msgs.msg import String
from blinkstick import blinkstick

def call_back(data):
	# rospy.loginfo(" LED color is %s", data.data)
	led = blinkstick.find_first()
	led.morph(name=data.data)

def led_off():
	led = blinkstick.find_first()
	led.turn_off()

def listner():
	rospy.init_node('listen_color', anonymous=True)
	rospy.Subscriber('led_color', String, call_back)
	rospy.loginfo("LED node is ready")
	rospy.on_shutdown(led_off)
	rospy.spin()

if __name__ == '__main__':
	listner()