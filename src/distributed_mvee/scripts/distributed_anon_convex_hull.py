#!/usr/bin/env python
import rospy
import os
import sys
from distributed_mvee.msg import AllData
from nav_msgs.msg import Odometry
from std_msgs.msg import String
import numpy as np
import time
from scipy.spatial import ConvexHull
import mvee_core

turtlebots = {'turtlebot00':0,'turtlebot01':1,'turtlebot02':2,'turtlebot03':3,'turtlebot04':4,'turtlebot05':5,'turtlebot06':6,'turtlebot07':7,'turtlebot08':8,'turtlebot09':9}


# mvee_coreset (P, epsilon, angle) 
# inputs: points P - 2 x N dimentaional matrix of [x,y]^T
# 		  epsilon = error in bounding ellipse, 0.001 is good!
# 		  angle - in degrees of the basis vectors : 0 <= angle < 90
# P = np.random.random_sample((2,100))
# (A,x0,coreset,core_ind) = mvee_core.mvee_coreset(P,0.001,45)

# Initialse the communication graph - for all turtlebots

# graph = {
# 	'turtlebot00':['turtlebot02','turtlebot07','turtlebot08','turtlebot09'],
# 	'turtlebot01':['turtlebot04','turtlebot06'],
# 	'turtlebot02':['turtlebot00','turtlebot04','turtlebot05'],
# 	'turtlebot03':['turtlebot04'],
# 	'turtlebot04':['turtlebot01','turtlebot02','turtlebot03'],
# 	'turtlebot05':['turtlebot02','turtlebot08'],
# 	'turtlebot06':['turtlebot01','turtlebot07'],
# 	'turtlebot07':['turtlebot00','turtlebot06'],
# 	'turtlebot08':['turtlebot00','turtlebot05','turtlebot09'],
# 	'turtlebot09':['turtlebot00','turtlebot08']
# }

graph = {
	'turtlebot00':['turtlebot06','turtlebot07','turtlebot08','turtlebot09'],
	'turtlebot06':['turtlebot00'],
	'turtlebot07':['turtlebot00'],
	'turtlebot08':['turtlebot00'],
	'turtlebot09':['turtlebot00']
}

class mvee_functions:

	def __init__(self):
		self.epsilon = 0.01
		self.angle = 0
		self.start_time = 0
		self.turtlebot_ID = os.environ.get('ROBOT')
		rospy.init_node(self.turtlebot_ID+'_mvee',anonymous=True)
		# self.initial_y = float('Inf')
		# self.initial_x = float('Inf')
		# self.initial_x = rospy.get_param('/'+self.turtlebot_ID+'_initial_x')
		# self.initial_y = rospy.get_param('/'+self.turtlebot_ID+'_initial_y')
		# self.initial_yaw = rospy.get_param('/'+self.turtlebot_ID+'_initial_yaw')
		# self.sleep_time = rospy.get_param('/'+self.turtlebot_ID+'_sleep')
		self.pub = rospy.Publisher('/yoshi/mvee', AllData, queue_size=1000)
		self.pub_led = rospy.Publisher('led_color', String,queue_size=1)
		self.rate = 0
		self.mvee_data = AllData()
		self.num_messages_sent = 0
		self.size_messages_sent = 0
		self.num_messages_received = 0
		self.size_messages_received = 0
		self.master = 'None'
		self.leader = turtlebots[self.turtlebot_ID]
		self.hop = 0
		self.coreset_ID_u = [turtlebots[self.turtlebot_ID]]
		self.coreset_x_u = [None]*1
		self.coreset_y_u = [None]*1
		self.A = np.array([]) # ellipse in matrix form
		self.x0 = np.array([]) # center of ellipse
		# self.coreset_ID_u.append(turtlebots[self.turtlebot_ID])
		# self.coreset_x_u.append(self.initial_x)
		# self.coreset_y_u.append(self.initial_y)
		self.coreset_ID = []
		self.coreset_x = []
		self.coreset_y = []
		rospy.Subscriber('/'+self.turtlebot_ID+'_vicon/odom', Odometry, self.callback_position)
		rospy.loginfo("Distributed Hull: Initial data has been loaded!")

	def initialize_mvee(self):
		# while self.initial_x == float('Inf') or self.pos_x == float('Inf'):
		# 	rospy.loginfo("Distributed Mvee: Waiting for position data")
		# 	self.pos_x = self.initial_x
		# 	self.pos_y = self.initial_y
		# Temporary fix to continue testing 
		self.coreset_ID = self.coreset_ID_u
		self.coreset_x = self.coreset_x_u
		self.coreset_y = self.coreset_y_u
		master_flag = False
		self.send_messages(master_flag,self.leader,self.hop,self.coreset_ID,self.coreset_x,self.coreset_y)
		rospy.loginfo("Distributed Hull: Initialization Done!")
		# self.set_led_color()
	# def callback_test(data):
	# 	rospy.loginfo("Distributed MVEE: Message received, hop = %d" %data.hop)
	# 	data.hop = data.hop + 1
	# 	pub.publish(data)

	def send_messages(self,master_flag,leader,hop,set_ID,set_x,set_y):
		# self.mvee_data.sender_ID = self.turtlebot_ID
		# self.mvee_data.leader = leader
		# self.mvee_data.hop = hop
		# self.mvee_data.set_ID = set_ID
		# self.mvee_data.set_x = set_x
		# self.mvee_data.set_y = set_y
		# self.mvee_data.from_workstation = True
		if master_flag:
			self.pub.publish(self.master,self.turtlebot_ID,leader,hop,set_ID,set_x,set_y,True)
			self.num_messages_sent+=1
			self.size_messages_sent+= self.get_size(self.master,self.turtlebot_ID,leader,hop,set_ID,set_x,set_y,True)
			# rospy.sleep(0.1)
		else:
			for children in graph[self.turtlebot_ID]:
				# self.mvee_data.receiver_ID = children
				self.pub.publish(children,self.turtlebot_ID,leader,hop,set_ID,set_x,set_y,True)
				self.num_messages_sent+=1
				self.size_messages_sent+= self.get_size(children,self.turtlebot_ID,leader,hop,set_ID,set_x,set_y,True)
				# rospy.sleep(0.1)

	def get_size(self,receiver_ID,sender_ID,leader,hop,set_ID,set_x,set_y,flag):
		size =  sys.getsizeof(set_x) + sys.getsizeof(set_y)
		return size

	def callback_position(self,data):
		# Transforming to world coordinate systems
		# theta = np.deg2rad(self.initial_yaw)
		# self.pos_x = data.pose.pose.position.x*np.cos(theta)-data.pose.pose.position.y*np.sin(theta) + self.initial_x
		# self.pos_y = data.pose.pose.position.x*np.sin(theta)+data.pose.pose.position.y*np.cos(theta) + self.initial_y
		
		# Get Position Data from Vicon
		self.coreset_x_u[0] = round(data.pose.pose.position.x,3)
		self.coreset_y_u[0] = round(data.pose.pose.position.y,3)

		if self.A.size:
			self.check_ellipse()
		# rospy.loginfo("Distributed MVEE: Position received (%f,%f) " %(self.pos_x,self.pos_y))
		# rospy.loginfo("Distributed Mvee: Position (%f,%f)"%(pos_x,pos_y))

	def check_ellipse(self):
		ellipse_point = np.array([self.coreset_x_u[0],self.coreset_y_u[0]])
		dist_from_ellipse = (ellipse_point-self.x0).dot(self.A).dot((ellipse_point-self.x0))
		if turtlebots[self.turtlebot_ID] in self.coreset_ID:
			if dist_from_ellipse > (1+self.epsilon)**2 or dist_from_ellipse < (1-self.epsilon)**2:
				self.coreset_x[self.coreset_ID.index(turtlebots[self.turtlebot_ID])] = self.coreset_x_u[0]
				self.coreset_y[self.coreset_ID.index(turtlebots[self.turtlebot_ID])] = self.coreset_y_u[0]
				# points = np.vstack((np.asfarray(self.coreset_x),np.asfarray(self.coreset_y)))
				# (self.A,self.x0,core_temp,coreid_temp) = mvee_core.mvee_coreset(points,self.epsilon,self.angle)
				# coreid_temp = coreid_temp.astype(int).tolist()
				# self.coreset_ID = [self.coreset_ID[i] for i in coreid_temp]
				# self.coreset_x = core_temp[0,:].tolist()
				# self.coreset_y = core_temp[1,:].tolist()

				#Send the messages
				master_flag = False
				self.send_messages(master_flag,self.leader,self.hop,self.coreset_ID,self.coreset_x,self.coreset_y)
				rospy.loginfo("Coreset Point is out of bounds")
		else:
			if dist_from_ellipse > (1+self.epsilon)**2:
				self.coreset_ID = self.coreset_ID_u + self.coreset_ID
				self.coreset_x = self.coreset_x_u + self.coreset_x
				self.coreset_y = self.coreset_y_u + self.coreset_y
				# points = np.vstack((np.asfarray(extended_x),np.asfarray(extended_y)))
				# (self.A,self.x0,core_temp,coreid_temp) = mvee_core.mvee_coreset(points,self.epsilon,self.angle)
				# coreid_temp = coreid_temp.astype(int).tolist()
				# self.coreset_ID = [extented_ID[i] for i in coreid_temp]
				# self.coreset_x = core_temp[0,:].tolist()
				# self.coreset_y = core_temp[1,:].tolist()

				#Send the messages
				master_flag = False
				self.send_messages(master_flag,self.leader,self.hop,self.coreset_ID,self.coreset_x,self.coreset_y)
				rospy.loginfo("Interior Point is out of ellipse")

	def callback_mvee(self,data):
		if (data.from_workstation):
			if (data.receiver_ID == self.turtlebot_ID):
				self.process_mvee(data)
				self.num_messages_received+=1
				self.size_messages_received+= self.get_size(data.receiver_ID,data.sender_ID,data.leader,data.hop,data.set_ID,data.set_x,data.set_y,data.from_workstation)
				rospy.loginfo('Message Received from: %s' %data.sender_ID)
				rospy.loginfo(self.turtlebot_ID+' | Messages received: %d | Messages sent: %d' % (self.num_messages_received,self.num_messages_sent))
				rospy.loginfo(self.turtlebot_ID+' | Size of messages received: %d | Size of messages sent: %d' %(self.size_messages_received,self.size_messages_sent))
				with open(self.turtlebot_ID+'_anon_CH-KY.txt','w') as text_file:
					text_file.write('Message Received from: %s \n' %data.sender_ID)
					text_file.write(self.turtlebot_ID+' | Messages received: %d | Messages sent: %d \n' % (self.num_messages_received,self.num_messages_sent))
					text_file.write(self.turtlebot_ID+' | Size of messages received: %d | Size of messages sent: %d' %(self.size_messages_received,self.size_messages_sent))

			# else:
				# rospy.loginfo("Message not for me! It is for %s" %data.receiver_ID)
		# self.set_led_color()

	def process_mvee(self,data):
		data.set_x = list(data.set_x)
		data.set_y = list(data.set_y)
		data.set_ID = list(data.set_ID)
		
		if not(set(self.coreset_ID) == set(data.set_ID)):
			data.set_x.extend(self.coreset_x)
			data.set_y.extend(self.coreset_y)
			data.set_ID.extend(self.coreset_ID)
			points = np.vstack((np.asfarray(data.set_x),np.asfarray(data.set_y)))

			if len(data.set_ID) <= 2:
				self.coreset_ID = data.set_ID
				self.coreset_x = data.set_x
				self.coreset_y = data.set_y
			else:
				hull = ConvexHull(points.transpose())
				temp_index = hull.vertices.tolist()
				try:
					self.coreset_ID = [data.set_ID[i] for i in temp_index]
				except IndexError:
					print "Leader"
					print self.leader
					print data.leader
					print "Hops"
					print self.hop
					print data.hop
					print "Coreset ID"
					print self.coreset_ID
					print "Data ID"
					print data.set_ID
					print "X and Y"
					print self.coreset_x
					print self.coreset_y
					print data.set_x
					print data.set_y
					print "Convex Hull indices"
					print temp_index
				
				self.coreset_x = points[0,hull.vertices].tolist()
				self.coreset_y = points[1,hull.vertices].tolist()
				master_flag = False
				self.send_messages(master_flag,self.leader,self.hop,self.coreset_ID,self.coreset_x,self.coreset_y)
		else:
			self.set_led_color()
			convergence_time = time.time() - self.start_time
			rospy.loginfo("Convergence Time is: %f" %convergence_time)
			with open(self.turtlebot_ID+'_anon_CH-KY_time.txt','w') as text_file:
				text_file.write("Convergence Time is: %f" %convergence_time)

	def set_led_color(self):
		points = np.vstack((np.asfarray(self.coreset_x),np.asfarray(self.coreset_y)))
		(self.A,self.x0,core_temp,coreid_temp) = mvee_core.mvee_coreset(points,self.epsilon,self.angle)
		coreid_temp = coreid_temp.astype(int).tolist()
		try:
			core_ID_actual = [self.coreset_ID[i] for i in coreid_temp]
		except IndexError:
			print "Temp index"
			print coreid_temp
			print "Convex Hull ID's"
			print self.coreset_ID
		if turtlebots[self.turtlebot_ID] in self.coreset_ID and turtlebots[self.turtlebot_ID] not in core_ID_actual:
			self.pub_led.publish("green")
		if turtlebots[self.turtlebot_ID] in core_ID_actual:
			self.pub_led.publish("blue")
		if turtlebots[self.turtlebot_ID] not in self.coreset_ID:
			self.pub_led.publish("red")

def mvee():
	# rospy.Subscriber('/'+mvee_obj.turtlebot_ID+'_vicon/odom', Odometry, mvee_obj.callback_position)
	rospy.Subscriber('/yoshi/mvee', AllData, mvee_obj.callback_mvee,queue_size=1000)
	while mvee_obj.pub.get_num_connections() < 6:
		pass
		# rospy.loginfo("Number of subscribers = %d" %mvee_obj.pub.get_num_connections())
	# rospy.sleep(mvee_obj.sleep_time) #Sleep and wait for topics to flip on workstation
	rospy.loginfo('Distributed Convex Hull: Hull is being initialised')
	mvee_obj.start_time = time.time()
	mvee_obj.initialize_mvee()
	# rospy.loginfo(' Distributed Mvee: Initialization done!')
	# num_messages_received_old = num_messages_received
	# while not rospy.is_shutdown():
	# if num_messages_received_old < num_messages_received:
	# 	rospy.loginfo(os.environ.get('ROBOT')+' | Messages received: %d' % num_messages_received)
	# 	num_messages_received_old = num_messages_received

	rospy.spin()

if __name__ == '__main__':
	mvee_obj = mvee_functions()
	mvee()
