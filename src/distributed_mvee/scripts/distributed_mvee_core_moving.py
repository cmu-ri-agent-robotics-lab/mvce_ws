#!/usr/bin/env python
import rospy
import os
import sys
from distributed_mvee.msg import AllData
from distributed_mvee.msg import ExcludeData
from nav_msgs.msg import Odometry
from std_msgs.msg import String
import numpy as np
import mvee_core
import time

turtlebots = {'turtlebot00':0,'turtlebot01':1,'turtlebot02':2,'turtlebot03':3,'turtlebot04':4,'turtlebot05':5,'turtlebot06':6,'turtlebot07':7,'turtlebot08':8,'turtlebot09':9}


# mvee_coreset (P, epsilon, angle) 
# inputs: points P - 2 x N dimentaional matrix of [x,y]^T
# 		  epsilon = error in bounding ellipse, 0.001 is good!
# 		  angle - in degrees of the basis vectors : 0 <= angle < 90
# P = np.random.random_sample((2,100))
# (A,x0,coreset,core_ind) = mvee_core.mvee_coreset(P,0.001,45)

# Initialse the communication graph - for all turtlebots

# graph = {
# 	'turtlebot00':['turtlebot02','turtlebot07','turtlebot08','turtlebot09'],
# 	'turtlebot01':['turtlebot04','turtlebot06'],
# 	'turtlebot02':['turtlebot00','turtlebot04','turtlebot05'],
# 	'turtlebot03':['turtlebot04'],
# 	'turtlebot04':['turtlebot01','turtlebot02','turtlebot03'],
# 	'turtlebot05':['turtlebot02','turtlebot08'],
# 	'turtlebot06':['turtlebot01','turtlebot07'],
# 	'turtlebot07':['turtlebot00','turtlebot06'],
# 	'turtlebot08':['turtlebot00','turtlebot05','turtlebot09'],
# 	'turtlebot09':['turtlebot00','turtlebot08']
# }

graph = {
	'turtlebot00':['turtlebot06','turtlebot07','turtlebot08','turtlebot09'],
	'turtlebot06':['turtlebot00'],
	'turtlebot07':['turtlebot00'],
	'turtlebot08':['turtlebot00'],
	'turtlebot09':['turtlebot00']
}

num_robots = 5


class mvee_functions:

	def __init__(self):
		self.epsilon = 0.01
		self.angle = 0
		self.start_time = 0
		self.turtlebot_ID = os.environ.get('ROBOT')
		rospy.init_node(self.turtlebot_ID+'_mvee',anonymous=True)
		# self.initial_y = float('Inf')
		# self.initial_x = float('Inf')
		# self.initial_x = rospy.get_param('/'+self.turtlebot_ID+'_initial_x')
		# self.initial_y = rospy.get_param('/'+self.turtlebot_ID+'_initial_y')
		# self.initial_yaw = rospy.get_param('/'+self.turtlebot_ID+'_initial_yaw')
		# self.sleep_time = rospy.get_param('/'+self.turtlebot_ID+'_sleep')
		self.pub = rospy.Publisher('/mvee', AllData, queue_size=1000)
		self.pub_exclude = rospy.Publisher('/mvee_exlude', ExcludeData, queue_size=1000)
		self.pub_led = rospy.Publisher('led_color', String,queue_size=1)
		self.rate = 0
		self.mvee_data = AllData()
		self.num_messages_sent = 0
		self.size_messages_sent = 0
		self.num_messages_received = 0
		self.size_messages_received = 0
		self.master = 'None'
		self.leader = turtlebots[self.turtlebot_ID]
		self.hop = 0
		self.coreset_ID_u = [turtlebots[self.turtlebot_ID]]
		self.coreset_x_u = [None]*1
		self.coreset_y_u = [None]*1
		self.coreset_time_u = [None]*1
		self.A = np.array([]) # ellipse in matrix form
		self.x0 = np.array([]) # center of ellipse
		# self.coreset_ID_u.append(turtlebots[self.turtlebot_ID])
		# self.coreset_x_u.append(self.initial_x)
		# self.coreset_y_u.append(self.initial_y)
		self.coreset_ID = []
		self.coreset_x = []
		self.coreset_y = []
		self.coreset_time = []
		self.coresetExclude = [None]*num_robots
		rospy.Subscriber('/mvee', AllData, self.callback_mvee,queue_size=1000)
		rospy.Subscriber('/'+self.turtlebot_ID+'_vicon/odom', Odometry, self.callback_position)
		rospy.Subscriber('/mvee_exlude', ExcludeData, self.callback_exclude,queue_size=1000)
		rospy.loginfo("Distributed Hull: Initial data has been loaded!")

	def initialize_mvee(self):
		# while self.initial_x == float('Inf') or self.pos_x == float('Inf'):
		# 	rospy.loginfo("Distributed Mvee: Waiting for position data")
		# 	self.pos_x = self.initial_x
		# 	self.pos_y = self.initial_y
		# Temporary fix to continue testing 
		self.coreset_ID = self.coreset_ID_u
		self.coreset_x = self.coreset_x_u
		self.coreset_y = self.coreset_y_u
		self.coreset_time = self.coreset_time_u
		self.A = np.array([]) 
		self.x0 = np.array([])
		master_flag = False
		priority = False
		self.send_messages(master_flag,self.leader,self.hop,self.coreset_ID,self.coreset_x,self.coreset_y,self.coreset_time,priority)
		rospy.loginfo("Distributed Hull: Initialization Done!")
	# def callback_test(data):
	# 	rospy.loginfo("Distributed MVEE: Message received, hop = %d" %data.hop)
	# 	data.hop = data.hop + 1
	# 	pub.publish(data)

	def send_messages(self,master_flag,leader,hop,set_ID,set_x,set_y,set_time,priority):
		if master_flag:
			self.pub.publish(self.master,self.turtlebot_ID,leader,hop,set_ID,set_x,set_y,set_time,priority)
			self.num_messages_sent+=1
			self.size_messages_sent+= self.get_size(self.master,self.turtlebot_ID,leader,hop,set_ID,set_x,set_y,set_time,priority)
		else:
			for children in graph[self.turtlebot_ID]:
				self.pub.publish(children,self.turtlebot_ID,leader,hop,set_ID,set_x,set_y,set_time,priority)
				self.num_messages_sent+=1
				self.size_messages_sent+= self.get_size(children,self.turtlebot_ID,leader,hop,set_ID,set_x,set_y,set_time,priority)

	def get_size(self,receiver_ID,sender_ID,leader,hop,set_ID,set_x,set_y,set_time,priority):
		size = sys.getsizeof(turtlebots[receiver_ID]) + sys.getsizeof(turtlebots[sender_ID]) + sys.getsizeof(leader) + sys.getsizeof(hop) + sys.getsizeof(set_ID) + sys.getsizeof(set_x) + sys.getsizeof(set_y) + sys.getsizeof(set_time) + sys.getsizeof(priority)
		return size

	def callback_exclude(self,data):
		

	def callback_position(self,data):
		# Transforming to world coordinate systems
		# theta = np.deg2rad(self.initial_yaw)
		# self.pos_x = data.pose.pose.position.x*np.cos(theta)-data.pose.pose.position.y*np.sin(theta) + self.initial_x
		# self.pos_y = data.pose.pose.position.x*np.sin(theta)+data.pose.pose.position.y*np.cos(theta) + self.initial_y
		
		# Get Position Data from Vicon
		self.coreset_x_u[0] = round(data.pose.pose.position.x,4)
		self.coreset_y_u[0] = round(data.pose.pose.position.y,4)
		self.coreset_time_u[0] = time.time()
		if turtlebots[self.turtlebot_ID] in self.coreset_ID:
			self.coreset_x[self.coreset_ID.index(turtlebots[self.turtlebot_ID])] = self.coreset_x_u[0]
			self.coreset_y[self.coreset_ID.index(turtlebots[self.turtlebot_ID])] = self.coreset_y_u[0]
			self.coreset_time[self.coreset_ID.index(turtlebots[self.turtlebot_ID])] = self.coreset_time_u[0]
		# else:
		# 	self.coreset_ID = self.coreset_ID_u + self.coreset_ID
		# 	self.coreset_x = self.coreset_x_u + self.coreset_x
		# 	self.coreset_y = self.coreset_y_u + self.coreset_y
		if self.A.size:
			self.check_ellipse()
		# rospy.loginfo("Distributed MVEE: Position received (%f,%f) " %(self.pos_x,self.pos_y))
		# rospy.loginfo("Distributed Mvee: Position (%f,%f)"%(pos_x,pos_y))

	def match_coreset(self,data):
		common_ID = list(set(self.coreset_ID).intersection(data.set_ID))
		for ID in common_ID:
			if data.set_time[data.set_ID.index(ID)] > self.coreset_time[self.coreset_ID.index(ID)]:
				self.coreset_x[self.coreset_ID.index(ID)] = data.set_x[data.set_ID.index(ID)]
				self.coreset_y[self.coreset_ID.index(ID)] = data.set_y[data.set_ID.index(ID)]
				self.coreset_time[self.coreset_ID.index(ID)] = data.set_time[data.set_ID.index(ID)]

	def make_unique(self):
		unique_ID = []
		unique_x = []
		unique_y = []
		unique_time = []
		for index,ID in enumerate(self.coreset_ID):
			if ID not in unique_ID:
				unique_ID.append(ID)
				unique_x.append(self.coreset_x[index])
				unique_y.append(self.coreset_y[index])
				unique_time.append(self.coreset_time[index])
		self.coreset_ID = unique_ID
		self.coreset_x = unique_x
		self.coreset_y = unique_y
		self.coreset_time = unique_time


	def check_ellipse(self):
		ellipse_point = np.array([self.coreset_x_u[0],self.coreset_y_u[0]])
		dist_from_ellipse = (ellipse_point-self.x0).dot(self.A).dot((ellipse_point-self.x0))
		if turtlebots[self.turtlebot_ID] in self.coreset_ID:
			if dist_from_ellipse > (1+self.epsilon)**2 or dist_from_ellipse < (1-self.epsilon)**2:
				# self.coreset_x[self.coreset_ID.index(turtlebots[self.turtlebot_ID])] = self.coreset_x_u[0]
				# self.coreset_y[self.coreset_ID.index(turtlebots[self.turtlebot_ID])] = self.coreset_y_u[0]
				# if dist_from_ellipse > (1+self.epsilon)**2:
				# 	self.coreset_x[self.coreset_ID.index(turtlebots[self.turtlebot_ID])] = self.coreset_x_u[0]
				# 	self.coreset_y[self.coreset_ID.index(turtlebots[self.turtlebot_ID])] = self.coreset_y_u[0]
				# if dist_from_ellipse < (1-self.epsilon)**2:
				# 	# while turtlebots[self.turtlebot_ID] in self.coreset_ID:
				# 	self.coreset_x.pop(self.coreset_ID.index(turtlebots[self.turtlebot_ID]))
				# 	self.coreset_y.pop(self.coreset_ID.index(turtlebots[self.turtlebot_ID]))
				# 	self.coreset_ID.remove(turtlebots[self.turtlebot_ID])
				# points = np.vstack((np.asfarray(self.coreset_x),np.asfarray(self.coreset_y)))
				# (self.A,self.x0,core_temp,coreid_temp) = mvee_core.mvee_coreset(points,self.epsilon,self.angle)
				# coreid_temp = coreid_temp.astype(int).tolist()
				# self.coreset_ID = [self.coreset_ID[i] for i in coreid_temp]
				# self.coreset_x = core_temp[0,:].tolist()
				# self.coreset_y = core_temp[1,:].tolist()

				# Send the reset messages
				master_flag = False
				exclude_from_coreset = True
				self.received_reset = True
				# self.A = np.array([]) # ellipse in matrix form
				# self.x0 = np.array([])
				self.send_messages(master_flag,self.leader,self.hop,self.coreset_ID_u,self.coreset_x_u,self.coreset_y_u,exclude_from_coreset)
				rospy.loginfo(self.turtlebot_ID+" is out of bounds")
				self.initialize_mvee()
		else:
			if dist_from_ellipse > (1+self.epsilon)**2:
				self.coreset_ID = self.coreset_ID_u + self.coreset_ID
				self.coreset_x = self.coreset_x_u + self.coreset_x
				self.coreset_y = self.coreset_y_u + self.coreset_y
				points = np.vstack((np.asfarray(extended_x),np.asfarray(extended_y)))
				(self.A,self.x0,core_temp,coreid_temp) = mvee_core.mvee_coreset(points,self.epsilon,self.angle)
				coreid_temp = coreid_temp.astype(int).tolist()
				self.coreset_ID = [extented_ID[i] for i in coreid_temp]
				self.coreset_x = core_temp[0,:].tolist()
				self.coreset_y = core_temp[1,:].tolist()
				master_flag = False
				exclude_from_coreset = False
				self.send_messages(master_flag,self.leader,self.hop,self.coreset_ID,self.coreset_x,self.coreset_y,exclude_from_coreset)

				# Send the reset messages
				# master_flag = False
				# exclude_from_coreset = True
				# self.received_reset = True
				# self.A = np.array([]) # ellipse in matrix form
				# self.x0 = np.array([])
				# self.send_messages(master_flag,self.leader,self.hop,self.coreset_ID_u,self.coreset_x_u,self.coreset_y_u,exclude_from_coreset)
				rospy.loginfo(self.turtlebot_ID+" moved outside")


	def callback_mvee(self,data):
		if (data.receiver_ID == self.turtlebot_ID):
			self.process_mvee(data)
			self.num_messages_received+=1
			self.size_messages_received+= self.get_size(data.receiver_ID,data.sender_ID,data.leader,data.hop,data.set_ID,data.set_x,data.set_y,data.from_workstation)
			rospy.loginfo('Message Received from: %s' %data.sender_ID)
			rospy.loginfo(self.turtlebot_ID+' | Messages received: %d | Messages sent: %d' % (self.num_messages_received,self.num_messages_sent))
			rospy.loginfo(self.turtlebot_ID+' | Size of messages received: %d | Size of messages sent: %d' %(self.size_messages_received,self.size_messages_sent))
			with open(self.turtlebot_ID+'_KY.txt','w') as text_file:
				text_file.write('Message Received from: %s \n' %data.sender_ID)
				text_file.write(self.turtlebot_ID+' | Messages received: %d | Messages sent: %d \n' % (self.num_messages_received,self.num_messages_sent))
				text_file.write(self.turtlebot_ID+' | Size of messages received: %d | Size of messages sent: %d' %(self.size_messages_received,self.size_messages_sent))

	def process_mvee(self,data):
		if (self.leader > data.leader) or ((self.leader==data.leader) and (self.hop>data.hop+1)):
			self.leader = data.leader
			self.hop = data.hop + 1
			self.master = data.sender_ID
			self.coreset_ID = self.coreset_ID_u
			self.coreset_x = self.coreset_x_u
			self.coreset_y = self.coreset_y_u
			master_flag = False
			self.send_messages(master_flag,self.leader,self.hop,self.coreset_ID,self.coreset_x,self.coreset_y)
		elif (self.leader == data.leader) and (self.hop < data.hop):
			# points = np.hstack(((np.vstack((self.coreset_x,self.coreset_y)),np.vstack((np.asarray(data.set_x),np.asarray(data.set_y))))))
			# self.coreset_x = list(set(self.coreset_x.extend(list(data.set_x))))
			# self.coreset_y = list(set(self.coreset_y.extend(list(data.set_y))))
			# self.coreset_ID = list(set(self.coreset_ID.extend(list(data.set_ID))))
			data.set_x = list(data.set_x)
			data.set_y = list(data.set_y)
			data.set_ID = list(data.set_ID)
			data.set_time= list(data.set_time)

			#Making a copy of incoming data - will be used later!
			data_copy = data

			data.set_x.extend(self.coreset_x)
			data.set_y.extend(self.coreset_y)
			data.set_ID.extend(self.coreset_ID)
			data.set_x.extend(self.coreset_x_u)
			data.set_y.extend(self.coreset_y_u)
			data.set_ID.extend(self.coreset_ID_u)
			points = np.vstack((np.asfarray(data.set_x),np.asfarray(data.set_y)))
			(self.A,self.x0,core_temp,coreid_temp) = mvee_core.mvee_coreset(points,self.epsilon,self.angle)
			coreid_temp = coreid_temp.astype(int).tolist()
			try:
				self.coreset_ID = [data.set_ID[i] for i in coreid_temp]
			except IndexError:
				print "Leader"
				print self.leader
				print data.leader
				print "Hops"
				print self.hop
				print data.hop
				print "Coreset ID"
				print self.coreset_ID
				print "Data ID"
				print data.set_ID
				print "X and Y"
				print self.coreset_x
				print self.coreset_y
				print data.set_x
				print data.set_y
				print "Mvee Coreset output"
				print core_temp
				print coreid_temp
			self.coreset_x = core_temp[0,:].tolist()
			self.coreset_y = core_temp[1,:].tolist()

			#For some reason numpy is not returning unique coreset from mvee_core
			#This function handles uniqueness in lists
			self.make_unique()

			self.match_coreset(data_copy)
			# This is the only odd message and cannot use self here because all messages are async
			self.pub.publish(data.sender_ID,self.turtlebot_ID,self.leader,self.hop,self.coreset_ID,self.coreset_x,self.coreset_y,True)
			self.num_messages_sent+=1
			self.size_messages_sent+= self.get_size(data.sender_ID,self.turtlebot_ID,self.leader,self.hop,self.coreset_ID,self.coreset_x,self.coreset_y,True)
			
			if not(self.master=='None'):
				master_flag = True
				self.send_messages(master_flag,self.leader,self.hop,self.coreset_ID,self.coreset_x,self.coreset_y)
		elif (self.leader==data.leader) and (self.master==data.sender_ID) and not(set(self.coreset_ID) == set(list(data.set_ID))):
			data.set_x = list(data.set_x)
			data.set_y = list(data.set_y)
			data.set_ID = list(data.set_ID)
			data.set_x.extend(self.coreset_x_u)
			data.set_y.extend(self.coreset_y_u)
			data.set_ID.extend(self.coreset_ID_u)
			points = np.vstack((np.asfarray(data.set_x),np.asfarray(data.set_y)))
			(self.A,self.x0,core_temp,coreid_temp) = mvee_core.mvee_coreset(points,self.epsilon,self.angle)
			coreid_temp = coreid_temp.astype(int).tolist()
			self.coreset_ID = [data.set_ID[i] for i in coreid_temp]
			self.coreset_x = core_temp[0,:].tolist()
			self.coreset_y = core_temp[1,:].tolist()
			master_flag = False
			self.send_messages(master_flag,self.leader,self.hop,self.coreset_ID,self.coreset_x,self.coreset_y)
		elif (self.leader==data.leader) and (set(self.coreset_ID) == set(list(data.set_ID))):
			self.set_led_color()
			convergence_time = time.time() - self.start_time
			rospy.loginfo("Convergence Time is: %f" %convergence_time)
			with open(self.turtlebot_ID+'_KY_time.txt','w') as text_file:
				text_file.write("Convergence Time is: %f" %convergence_time)

	def set_led_color(self):
		if turtlebots[self.turtlebot_ID] in self.coreset_ID:
			self.pub_led.publish("green")
		else:
			self.pub_led.publish("red")

def mvee():
	while mvee_obj.pub.get_num_connections() < 6:
		pass
		# rospy.loginfo("Number of subscribers = %d" %mvee_obj.pub.get_num_connections())
	# rospy.sleep(mvee_obj.sleep_time) #Sleep and wait for topics to flip on workstation
	rospy.loginfo('Distributed MVEE: MVEE is being initialised')
	mvee_obj.start_time = time.time()
	mvee_obj.initialize_mvee()
	# rospy.loginfo(' Distributed Mvee: Initialization done!')
	# num_messages_received_old = num_messages_received
	# while not rospy.is_shutdown():
	# if num_messages_received_old < num_messages_received:
	# 	rospy.loginfo(os.environ.get('ROBOT')+' | Messages received: %d' % num_messages_received)
	# 	num_messages_received_old = num_messages_received

	rospy.spin()

if __name__ == '__main__':
	mvee_obj = mvee_functions()
	mvee()
