#!/usr/bin/env python
import rospy
from distributed_mvee.msg import AllData
from nav_msgs.msg import Odometry
from std_msgs.msg import String
import os
import time
import random
from multiprocessing.dummy import Process
import numpy as np
import mvee_core
import matplotlib.pyplot as plt


num_turtlebots = 10
# all_positions_x = [random.random() for bot in range(num_turtlebots)]
# all_positions_y = [random.random() for bot in range(num_turtlebots)]
all_positions_x = [0.0]*10
all_positions_y = [0.0]*10
epsilon = 0.01
angle = 0
plot_done = [False]
pub_led = []

def plot(pub_led):
	core_ind_old = []
	while not plot_done[0]:
		points = np.vstack((np.asfarray(all_positions_x),np.asfarray(all_positions_y)))
		(A,x0,coreset,core_ind) = mvee_core.mvee_coreset(points,epsilon,angle)
		# print "A = "
		# print A
		# print "Center: "
		# print x0
		# print "Coreset Points:"
		# print coreset
		# print "Indices of coreset points:"
		# print core_ind
		if not(A.size):
			continue

		plt.plot(points[0,:],points[1,:],'ro')
		t = np.linspace(0,2*np.pi,num=100)
		D,V = np.linalg.eig(A)
		e=np.tile(x0,(len(t),1)).transpose() + np.dot(V.dot(np.diag(1./np.sqrt(D))),np.vstack((np.array([np.cos(t)]),np.array([np.sin(t)]))))
		plt.plot(e[0,:],e[1,:],'b-')
		plt.plot(coreset[0,:],coreset[1,:],'go')
		for i,txt in enumerate(range(num_turtlebots)):
			plt.annotate(txt,(points[0,i],points[1,i]))
		plt.draw()
		plt.show(block=False)
		plt.clf()
		if set(core_ind) != set(core_ind_old):
			green_leds = set(core_ind)
			red_leds = [led for led in range(num_turtlebots) if led not in green_leds]
			for i in green_leds:
				pub_led[int(i)].publish("green")
			for i in red_leds:
				pub_led[int(i)].publish("red")
			core_ind_old = core_ind

	plt.close("all")

def callback(data,robot_id):
	all_positions_x[robot_id] = data.pose.pose.position.x
	all_positions_y[robot_id] = data.pose.pose.position.y


def main():
	rospy.init_node("Workstation_"+os.environ.get('ROBOT'),anonymous=True)
	#Subscribe to all postions
	# rospy.Subscriber(os.environ.get('ROBOT')+'/mvee', AllData, callback,queue_size=1000)
	for k in range(num_turtlebots):
		rospy.Subscriber('/turtlebot0'+str(k)+'_vicon/odom', Odometry, callback, callback_args=k)
		pub_led.append(rospy.Publisher('/turtlebot0'+str(k)+'/led_color', String, queue_size=1))
	rospy.loginfo("Workstation Node Initialized!")
	p = Process(target = plot(pub_led))
	p.start()
	rospy.spin()
	plot_done[0] = True
	p.join()

if __name__=="__main__":
	main()