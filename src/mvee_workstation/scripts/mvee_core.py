#!/usr/bin/env python
import numpy as np
import matplotlib.pyplot as plt

def khachiyan(a,tol):
	(n,m) = a.shape
	invA = m*np.linalg.inv(np.dot(a,a.transpose()))
	w = np.sum(a*np.dot(invA,a),axis=0)
	while True:
		w_r = np.max(w)
		r = np.argmax(w)
		f = w_r/n
		epsilon = f-1
		if epsilon<=tol:
			break
		g = epsilon/((n-1)*f)
		h = 1+g
		g = g/f
		b = np.dot(invA,a[:,r:r+1])
		invA = h*invA-g*np.dot(b,b.T) 
		bta = np.dot(b.T,a) 
		w = h*w - g*bta*bta 
	E = invA/w_r;
	return E

def lowner(points,tol):
	# points in 2xN matix of positions
	# tol is error tolerance
	# returns ellipse in the for of A - an 2x2 matrix and its center c
	(n,m) = points.shape
	points_h = np.ones((n+1,m))
	points_h[:-1,:] = points
	F = khachiyan(points_h,tol)
	A = F[0:n,0:n]
	b = F[0:n,-1]
	c = -np.linalg.solve(A,b)
	E = A / (1-np.dot(c,b)-F[-1,-1])
	ac = points - np.tile(c,(m,1)).transpose()
	E = E / np.max(np.sum(ac*np.dot(E,ac),axis=0))

	return E,c

def get_direction(angle):
	theta = (angle/180.)*np.pi
	basis_direction = np.array([[np.cos(theta), -np.sin(theta)],[np.sin(theta),np.cos(theta)]])
	return basis_direction

def mvee_coreset(P,epsilon,angle):
	# P is positions of robots in 2xN - N robots in 2D
	(d,n) = P.shape
	A = np.array([])
	x0 = np.array([])
	core_ind = np.array([])
	Q = []
	if n <= 2*d -1:
		# find unique robot positions - need to prevent singularity
		# need to transpose for this to work properly
		P = P.transpose()
		temp = np.ascontiguousarray(P).view(np.dtype((np.void, P.dtype.itemsize * P.shape[1])))
		_, core_ind = np.unique(temp, return_index=True)
		coreset = P[core_ind]
		coreset = coreset.transpose()
		P = P.transpose()
		
		return A,x0,coreset,core_ind
	else:
		# angle = 45 # Angle of basis vectors relative to primary axes - 0 to 90 degrees is only required
		direction = get_direction(angle) # any direction can be used
		for i in range(d):
			b = direction[:,i]
			proj_vec = np.dot(b,P)
			max_value = np.max(proj_vec)
			max_index = np.argmax(proj_vec)
			core_ind = np.append(core_ind,max_index)
			Q.append(P[:,max_index])
			min_value = np.min(proj_vec)
			min_index = np.argmin(proj_vec)
			core_ind = np.append(core_ind,min_index)
			Q.append(P[:,min_index])

	Q = np.array(Q)
	temp = np.ascontiguousarray(Q).view(np.dtype((np.void, Q.dtype.itemsize * Q.shape[1])))
	_, temp_ind = np.unique(temp, return_index=True)
	Q_unique = Q[temp_ind]
	core_ind_unique = core_ind[temp_ind]
	Q_unique = Q_unique.transpose()
	Q = Q.transpose()

	if Q_unique.shape[1] < Q.shape[1] -1:
		print "Problem Q is singular"
		# continue
		return A,x0,Q_unique,core_ind_unique
	else:
		# Ellipse calculation and then getting coreset
		# for i in range(int(np.ceil(d*np.log(d)+d/epsilon))):
		while True:
			(A,x0) = lowner(Q,epsilon)
			v = []
			for j in range(n):
				v.append(((P[:,j]-x0).dot(A)).dot((P[:,j]-x0)) - (1+epsilon)**2)
			v = np.array(v)
			max_v = np.max(v)
			max_vindex = np.argmax(v)
			if max_v < 0:
				return A,x0,Q,core_ind
			else:
				Q = Q.transpose()
				Q = np.vstack((Q,P[:,max_vindex]))
				core_ind = np.append(core_ind,max_vindex)
				Q = Q.transpose()

# Main testing if all mathods are okay!
# P = np.random.random_sample((2,100))
# (A,x0,coreset,core_ind) = mvee_coreset(P,0.001,60)
# print "A = "
# print A
# print "Center: "
# print x0
# print "Coreset Points:"
# print coreset
# print "Indices of coreset points:"
# print core_ind
# plt.plot(P[0,:],P[1,:],'ro')
# t = np.linspace(0,2*np.pi,num=100)
# D,V = np.linalg.eig(A)
# e=np.tile(x0,(len(t),1)).transpose() + np.dot(V.dot(np.diag(1./np.sqrt(D))),np.vstack((np.array([np.cos(t)]),np.array([np.sin(t)]))))
# plt.plot(e[0,:],e[1,:],'b-')
# plt.plot(coreset[0,:],coreset[1,:],'go')
# plt.show(block=False)
