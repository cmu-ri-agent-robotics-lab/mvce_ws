echo "Copying .bashrc to all TurtleBots..."
rsync -avz --del ~/ros/workspaces/hydro/mvce_ws/.bashrc turtlebot@192.168.1.59:~/ &
rsync -avz --del ~/ros/workspaces/hydro/mvce_ws/.bashrc turtlebot@192.168.1.58:~/ &
rsync -avz --del ~/ros/workspaces/hydro/mvce_ws/.bashrc turtlebot@192.168.1.57:~/ &
rsync -avz --del ~/ros/workspaces/hydro/mvce_ws/.bashrc turtlebot@192.168.1.56:~/ &
rsync -avz --del ~/ros/workspaces/hydro/mvce_ws/.bashrc turtlebot@192.168.1.55:~/ &
rsync -avz --del ~/ros/workspaces/hydro/mvce_ws/.bashrc turtlebot@192.168.1.54:~/ &
rsync -avz --del ~/ros/workspaces/hydro/mvce_ws/.bashrc turtlebot@192.168.1.53:~/ &
rsync -avz --del ~/ros/workspaces/hydro/mvce_ws/.bashrc turtlebot@192.168.1.52:~/ &
rsync -avz --del ~/ros/workspaces/hydro/mvce_ws/.bashrc turtlebot@192.168.1.51:~/ &
rsync -avz --del ~/ros/workspaces/hydro/mvce_ws/.bashrc turtlebot@192.168.1.50:~/ 
wait
echo "Finished copying bashrc"
echo "Copying ROS mvce workspace to all TurtleBots..."
rsync -avz --del ~/ros/workspaces/hydro/mvce_ws/src turtlebot@192.168.1.59:~/ros/mvce_ws/ &
rsync -avz --del ~/ros/workspaces/hydro/mvce_ws/src turtlebot@192.168.1.58:~/ros/mvce_ws/ &
rsync -avz --del ~/ros/workspaces/hydro/mvce_ws/src turtlebot@192.168.1.57:~/ros/mvce_ws/ &
rsync -avz --del ~/ros/workspaces/hydro/mvce_ws/src turtlebot@192.168.1.56:~/ros/mvce_ws/ &
rsync -avz --del ~/ros/workspaces/hydro/mvce_ws/src turtlebot@192.168.1.55:~/ros/mvce_ws/ &
rsync -avz --del ~/ros/workspaces/hydro/mvce_ws/src turtlebot@192.168.1.54:~/ros/mvce_ws/ &
rsync -avz --del ~/ros/workspaces/hydro/mvce_ws/src turtlebot@192.168.1.53:~/ros/mvce_ws/ &
rsync -avz --del ~/ros/workspaces/hydro/mvce_ws/src turtlebot@192.168.1.52:~/ros/mvce_ws/ &
rsync -avz --del ~/ros/workspaces/hydro/mvce_ws/src turtlebot@192.168.1.51:~/ros/mvce_ws/ &
rsync -avz --del ~/ros/workspaces/hydro/mvce_ws/src turtlebot@192.168.1.50:~/ros/mvce_ws/ 
wait
echo "Finished copying packages"